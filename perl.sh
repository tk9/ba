#!/bin/bash

#kill the perl webserver and restart - needed for change in pl file, not in template
for i in `netstat -anp | grep 3001 | awk {'print $7}' |awk -F '/' {'print $1'}| uniq` ; do kill $i ; done
/home/t/pe-app/bin/app.pl --port 3001&
curl http://localhost:3001
